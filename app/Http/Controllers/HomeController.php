<?php

namespace App\Http\Controllers;

use App\Http\Models\PostModel;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
    }

    public function indexAction()
    {
        $posts = PostModel::all();
        return view('home.index', compact('posts'))->withTitle('Home');
    }

    public function showAction($id)
    {
        $postModel = PostModel::find($id);
        return view('home.show', compact('postModel'))->withTitle('View');
    }

    public function editAction($id)
    {
        $postModel = PostModel::select('id', 'description')->where('id', $id)->first();
        if ($postModel instanceof PostModel) {
            return view('home.edit', compact('postModel'))->withTitle('View');
        }
        return view('home.edit')->withTitle('View');
    }

    public function createAction()
    {
        $postModel = new PostModel();
        return view('home.create', compact('postModel'))->withTitle('Create');
    }

    public function saveAction(Request $request)
    {
        if ($request->ajax()) {
            if ($post = PostModel::create($request->all())) {
                return response()->json(['msg' => 'success', 'redirect' => route('home-show', ['id' => $post->id])]);
            } else {
                return response()->json(['msg' => 'error', 404]);
            }
        } else {
            return response()->json(['msg' => 'error', 404]);
        }
    }

    public function updateAction(Request $request, $id)
    {
        if ($request->ajax()) {
            $postModel = PostModel::where('id', $id)->first();
            if ($postModel instanceof PostModel) {
                if ($postModel->update($request->all())) {
                    return response()->json(['msg' => 'success', 'redirect' => route('home-show', ['id' => $postModel->id])]);
                } else {
                    return response()->json(['msg' => 'error', 404]);
                }
            } else {
                return response()->json(['msg' => 'error', 404]);
            }
        } else {
            return response()->json(['msg' => 'error', 404]);
        }
    }

    public function deleteAction(Request $request, $id)
    {
        if ($request->ajax()) {
            $postModel = PostModel::find($id);
            if ($postModel->delete()) {
                return response()->json(['msg' => 'success', 'data' => $id], 200);
            } else {
                return response()->json(['msg' => 'error', 404]);
            }
        } else {
            return response()->json(['msg' => 'error', 404]);
        }

    }
}
