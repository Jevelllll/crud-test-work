<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * @property integer id
 * @property string description
 * @property string created_at
 * @property string update_at
 */
class PostModel extends Model
{

    protected $table = 'post';
    protected $fillable = ['description', 'created_at', 'update_at'];

}
