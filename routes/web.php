<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@indexAction')->name('home-index');
Route::get('/show/{id}', 'HomeController@showAction')->where('id', '[0-9]+')->name('home-show');
Route::get('/create', 'HomeController@createAction')->name('home-create');
Route::post('/save', 'HomeController@saveAction')->name('home-save');
Route::get('/edit/{id}', 'HomeController@editAction')->where('id', '[0-9]+')->name('home-edit');
Route::put('/update/{id}', 'HomeController@updateAction')->where('id', '[0-9]+')->name('home-update');
Route::delete('/delete/{id}', 'HomeController@deleteAction')->name('home-delete');
