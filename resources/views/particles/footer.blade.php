<footer class="page-footer no-padding purple darken-4">
    <div class="footer-copyright">
        <div class="container">
            © {{date('Y')}}
        </div>
    </div>
</footer>
