<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CRUD</title>
    <link rel="preload" href="{{asset('css/app.css')}}?v=<?= filemtime('css/app.css')?>" as="style">
    <link href="{{asset('css/app.css')}}?v=<?= filemtime('css/app.css')?>" rel="stylesheet">
    {{--materialize cstyle--}}
    <link rel="stylesheet"
          href="{{asset('css/materialize/materialize.css')}}?v=<?= filemtime('css/materialize/materialize.css') ?>">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {{-- ----- --}}
</head>
<body>
@include('particles.navbar')
<main>
    @yield('content')
</main>
@include('modals.delete-confirm')
@include('particles.footer')
<script defer
        src="{{ asset('js/base.js') }}?v=<?= filemtime('js/base.js') ?>"></script>
<script defer
        src="{{ asset('js/materialize/materialize.js') }}?v=<?= filemtime('js/materialize/materialize.js') ?>"></script>
</body>
</html>
