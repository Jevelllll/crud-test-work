<div id="delete-modal" class="modal">
    <div class="modal-content">
        <h4>Delete confirmation</h4>
        <p>Are you sure you want to delete the entry?</p>
    </div>
    <div class="modal-footer">
        <button class="agree-remove waves-effect waves-green btn-flat red white-text">Agree</button>
        <button class="not-remove waves-effect waves-green btn-flat">Not</button>
    </div>
</div>
