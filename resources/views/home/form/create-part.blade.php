<div class="input-field">
    <i class="material-icons prefix">mode_edit</i>
    {{Form::label('description', 'Description', ['for' => 'description'])}}
    {{Form::textarea('description',  null, ['id' => 'description', 'maxlength'=> '120','rows' => 2, 'cols' => 40,'class' => 'materialize-textarea','data-length'=>'120'])}}
</div>
@if($postModel instanceof \App\Http\Models\PostModel)
    @if(count((array) $postModel->description) > 0)
        <button  id="sendCreate" class="btn btn-large waves-effect waves-light w-100" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
        </button>
    @else
        <button  id="sendCreate" class="btn btn-large waves-effect waves-light w-100 disabled" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
        </button>
    @endif
@else
    <button id="sendCreate" class="btn btn-large waves-effect waves-light w-100 disabled" type="submit" name="action">Submit
        <i class="material-icons right">send</i>
    </button>
@endif
