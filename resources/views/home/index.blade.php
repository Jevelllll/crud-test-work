@extends('particles.layout')
@section('content')
    <div class="container">
        <a href="{{route('home-create')}}" class="add-note btn-floating btn-large waves-effect waves-light red p4">
            <i class="material-icons">add</i>
        </a>
        <div class="content-height purple lighten-5 scrollable">
            @if(count((array) $posts) > 0)
            <div class="row">
                    @foreach($posts as $post)
                        <div class="col s12 m6" data-id="{{$post->id}}">
                            <div class="card-panel highlighted deep-purple">
                                <div class="card-radio">
                                    <a href="{{route('home-edit', ['id' => $post->id])}}"><i class="material-icons">edit</i></a>
                                    <a href="{{route('home-show', ['id' => $post->id])}}"><i class="material-icons">pageview</i></a>
                                    <button data-url="{{route('home-delete', $post->id)}}"
                                            class="remove-data unstyling-button blue-text darken-1"><i
                                            class="material-icons">delete_forever</i></button>
                                </div>
                                <div class="white-text">
                                    <span>{{$post->description}}</span>
                                    <div class="divider"></div>
                                    <p class="s2">{{$post->created_at}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
            </div>
            @else
                <div class="card-panel teal lighten-2 white-text">There is no data</div>
            @endif
        </div>
    </div>
@endsection
