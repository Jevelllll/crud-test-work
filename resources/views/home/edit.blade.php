@extends('particles.layout')
@section('content')
    <h1 class="center">Update</h1>
    <div class="container">
        @if($postModel instanceof \App\Http\Models\PostModel)
            {{Form::model($postModel, ['route' => ['home-update', $postModel->id],
                  'id'=>'postForm', 'class'=> 'col s6', 'method' => 'PUT', 'enctype'=>'multipart/form-data']) }}
            @include('home.form.create-part')
            {{ Form::close() }}
        @else
            <div class="card-panel teal lighten-2 white-text">There is no data</div>
        @endif
    </div>
@endsection
