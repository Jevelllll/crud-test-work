@extends('particles.layout')
@section('content')
    <h1 class="center">Create</h1>
    <div class="container">
        {{Form::model($postModel, array('route' => array('home-save'),
                   'id'=>'postForm', 'class'=> 'col s6', 'method' => 'POST', 'enctype'=>'multipart/form-data')) }}
        @include('home.form.create-part')
        {{ Form::close() }}
    </div>
@endsection
