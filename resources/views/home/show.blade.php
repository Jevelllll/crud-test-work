@extends('particles.layout')
@section('content')
    <h1 class="center">Read</h1>
    <div class="container">
        @if($postModel instanceof \App\Http\Models\PostModel)
            <ul class="collection with-header">
                <?php $newCollectPost = collect($postModel)->toArray();?>
                @foreach($newCollectPost as $postKey => $postVal)
                    <li class="collection-item">
                        <div>{{$postKey}}<span class="secondary-content">{{$postVal}}</span>
                        </div>
                    </li>
                @endforeach
            </ul>
        @else
            <div class="card-panel teal lighten-2 white-text">There is no data</div>
        @endif
    </div>
@endsection
