const skipUrl = ['/', 'show'];
let removeUrl;
let instanceModalRemove;
document.addEventListener('DOMContentLoaded', function () {
    let pathname = window.location.pathname;
    let textArea = document.querySelector('.materialize-textarea');
    M.CharacterCounter.init(textArea);

    if (!skipUrl.includes(pathname)) {
        textAreaValidLength();
        sendPostForm();
    } else {
        removeConfirm();
    }

});

function removeConfirm() {
    let listOf = document.querySelectorAll('.remove-data');
    [].slice.call(listOf).forEach(sendRemove);

    let agreeRemove = document.querySelector('.agree-remove');
    let notRemove = document.querySelector('.not-remove');
    agreeRemove.addEventListener('click', function (ev) {
        ev.preventDefault()
        sendRequestServer('DELETE', removeUrl, null, (result) => {
            const id = result.data;
            let currentCard = document.querySelector(`[data-id="${id}"]`);
            currentCard.remove();
            M.toast({html: 'Successfully deleted!'})
            instanceModalRemove.close();
        });
    });
    notRemove.addEventListener('click', () => {
        instanceModalRemove.close();
    });
}

function sendRemove(el) {
    const elems = document.getElementById('delete-modal');
    instanceModalRemove = M.Modal.init(elems);
    if (el) el.addEventListener('click', (dataUrl) => {
        removeUrl = el.getAttribute('data-url');
        instanceModalRemove.open();
    });
}

function sendRequestServer(method, url, data = null, cd) {
    let httpRequest = new XMLHttpRequest();
    httpRequest.open(method, url, true);
    if (method === 'POST' || method === 'DELETE') {
        httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    }
    httpRequest.setRequestHeader(
        'X-CSRF-TOKEN',
        document.querySelector("meta[name='csrf-token']").getAttribute('content'));
    httpRequest.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    httpRequest.responseType = 'json';
    if (data) {
        let dataUrlencoded;
        if (typeof data === 'string') {
            dataUrlencoded = data;
        } else {
            dataUrlencoded = constructUrlencoded(data);
        }
        httpRequest.send(dataUrlencoded);
    } else {
        httpRequest.send();
    }
    httpRequest.onload = () => {
        if (httpRequest.status === 200) {
            cd(httpRequest.response)
        } else {
            cd(false)
        }
    };
    httpRequest.onerror = () => {
        cd(false)
    }
}

// Sending form to server
function sendPostForm() {
    let postForm = document.getElementById('postForm');
    if (!postForm) return false;
    postForm.addEventListener('submit', (el) => {
        el.preventDefault();
        let targetForm = el.target;
        sendRequestServer(
            targetForm.getAttribute('method'),
            targetForm.getAttribute('action'),
            targetForm.serialize(),
            (result) => {
                M.toast({html: 'Successfully!'})
                setTimeout(() => {
                    window.location.href = result.redirect;
                }, 200)
            }
        )
    })
}


// subscribe to change value
function textAreaValidLength() {
    let sendCreate = document.getElementById('sendCreate');
    let textArea = document.querySelector('#description');
    if (textArea) {
        textArea.addEventListener('keyup', (el) => {
            let target = el.target;
            if (target) {
                let value = target.value;
                if (value.length > 0) {
                    if (sendCreate.classList.contains('disabled')) {
                        sendCreate.classList.remove('disabled');
                    }
                } else {
                    if (!sendCreate.classList.contains('disabled')) {
                        sendCreate.classList.add('disabled');
                    }
                }
            }
        })
    }
    return false;
}

function constructUrlencoded(data) {
    if (Array.isArray(data)) {
        if (data.length > 0) {
            return data.map((el) => {
                let fields = '';
                Object.keys(el).forEach((field) => {
                    fields = `${field}=${el[field]}`;
                });
                return fields;
            }).filter((it) => it).join('&')
        } else {
            return false;
        }
    }
    return false;
}

HTMLElement.prototype.serialize = function () {
    let requestArray = [];
    this.querySelectorAll('[name]').forEach((elem) => {
        if (elem.name !== 'action') requestArray.push(elem.name + '=' + elem.value);
    });
    if (requestArray.length > 0)
        return requestArray.join('&');
    else
        return false;
}
